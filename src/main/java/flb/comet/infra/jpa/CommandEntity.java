package flb.comet.infra.jpa;

import flb.comet.domain.Client;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "command")
@Data
@EqualsAndHashCode(of = "id",callSuper = false)
public class CommandEntity extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<PizzaEntity> pizzas;

    @Column(name = "client")
    @JdbcTypeCode(SqlTypes.JSON)
    private Client client;
}
