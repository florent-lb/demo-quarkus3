package flb.comet;

import flb.comet.infra.jpa.CommandRepository;
import flb.comet.infra.jpa.Initiator;
import flb.comet.infra.jpa.PizzaEntity;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Path("/pizza")
public class ExampleResource {

    @Inject
    Initiator initiator;
    @Inject
    CommandRepository repository;

    @GET
    @Path("/init")
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return initiator.init();
    }

    @GET
    @Path("/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    public List<PizzaEntity> createAndList(@PathParam("name") String name) throws ExecutionException, InterruptedException {
        return repository.createPizza(name).thenApply(pizzaEntity -> PizzaEntity.<PizzaEntity>findAll().list()).get();
    }
    @GET
    @Path("/classics")
    @Produces(MediaType.TEXT_PLAIN)
    @Transactional
    public List<PizzaEntity> later()  {
        return repository.getTheClassics();
    }

}
