package flb.comet.infra.jpa;

import flb.comet.domain.Client;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.random.RandomGenerator;

@RequestScoped
public class Initiator {

    @Inject
    EntityManager entityManager;


    @Transactional
    public String init() {
        RandomGenerator generator = new SecureRandom();
        final var nbCommands = generator.nextInt(5, 10);

        final Client kevin = new Client("Kevin", 5L);

        PizzaEntity chevre = new PizzaEntity();
        chevre.setName("Chevre");
        PizzaEntity tartiflette = new PizzaEntity();
        tartiflette.setName("Tartiflette");

        entityManager.persist(chevre);
        entityManager.persist(tartiflette);

        for (int i = 0; i < nbCommands; i++) {
            final var pizzaOdd = generator.nextInt(0, 20) % 2;
            CommandEntity c = new CommandEntity();
            c.setClient(kevin);
            ArrayList pizzas = new ArrayList();
            pizzas.add(tartiflette);
            if (pizzaOdd == 0) pizzas.add(chevre);

            c.setPizzas(pizzas);
            entityManager.persist(c);


        }
        return "Create a lot of pizza !";
    }
}
