package flb.comet.infra.jpa;

import io.quarkus.panache.common.Parameters;
import jakarta.ejb.Asynchronous;
import jakarta.enterprise.context.Dependent;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import lombok.extern.java.Log;
import org.hibernate.Session;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import static java.util.concurrent.CompletableFuture.completedFuture;

@Dependent
@Log
public class CommandRepository {

    @Inject
    Session entityManager;

    @Asynchronous
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public CompletableFuture<PizzaEntity> createPizza(String name) throws InterruptedException {

        log.info("Préparation des ingrédients pour la %s".formatted(name));
        Thread.sleep(2000L);
        PizzaEntity newPizze = new PizzaEntity();
        log.info("Cuisson de la %s".formatted(name));
        newPizze.setName(name);
        log.info("Pizza %s ajoutée".formatted(name));
        entityManager.persist(newPizze);
        Thread.sleep(2000L);
        return completedFuture(newPizze);
    }


    @Transactional
    public List<PizzaEntity> getTheClassics() {
        return entityManager.createQuery("""
                SELECT classics.nameDerived
                FROM (
                    SELECT p.name as nameDerived
                    FROM PizzaEntity p
                    WHERE p.name LIKE 'C%'
                ) classics
                """, PizzaEntity.class).getResultList();

    }


}
