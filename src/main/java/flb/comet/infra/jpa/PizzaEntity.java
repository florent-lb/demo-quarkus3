package flb.comet.infra.jpa;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.smallrye.common.constraint.NotNull;
import io.vertx.core.spi.launcher.Command;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "pizza")
@Data
@EqualsAndHashCode(of = "id", callSuper = false)
public class PizzaEntity extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;
    @Column(name = "name", length = 50, nullable = false)
    @NotNull
    private String name;
    @ManyToMany(cascade = CascadeType.ALL)
    private List<CommandEntity> commands;

    @Override
    public String toString() {
        return name + " (%s)".formatted(id);
    }
}
